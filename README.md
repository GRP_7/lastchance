

## Food Inc.



* Built using [Laravel](https://laravel.com/) with MVC architectural pattern 

Following is an explanation on how to use and check our files. 

*Needed: 

PHP 7.2.11 

MySQL 8.0 - we have added support for older versions but 8.0 is recommended.*

---

## Locate files
*Explanation on how to use the files will follow below this section*

**\routes\web.php** is the file for routing.

**\resources\views** is where the View layer are stored and routed from. 

**\public\css** is where .css files are stored.

**\public\js** is where .js files are stored.

**\public\images** is where image files are stored.

**\database\migrations** is where .files for building the database is stored.

**\database\seeds** is where DatabaseSeeder.php is found. 

**\app** is where the Model layer are stored.

**\app\Http\Controllers** is where the Controller layer are stored.


---

## Using/running the site

1. Create a new local database and change the DB_ properties in the **.env** file in **\\** to your local session.
2. Open some kind of CLI and go to **\\** where you can find the artisan file.
3.  Run command: php artisan migrate **Make sure you have changed the .env file since this will migrate over the database Food Inc. is using.**
4. Run command: php artisan [yourLocalDB]:seed
5. Run command: php artisan serve
6. Open a browser and go to localhost:8000.
---