<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ingredients_recipe extends Model
{
	public $timestamps = false;

	protected $fillable = [
		'URL_Recipe',
		'Ingredients'
	];
}
