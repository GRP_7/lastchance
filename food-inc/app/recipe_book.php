<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class recipe_book extends Model
{
	public $timestamps = false;

	protected $fillable = [
		'URL',
		'Label',
		'Image',
		'Calories',
	];
}
