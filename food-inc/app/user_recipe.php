<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_recipe extends Model
{
	public $timestamps = false;

	protected $fillable = [
		'Owner_ID',
		'Recipe_URL',
		'Available',
	];
}
