<?php

namespace App\Http\Controllers;

class LogOutController extends Controller
{
	public function logout()
	{
		session(['logged_in' => false,
				 'id' => null,
				 'clearance_level' => null]);

		return view('index');
	}
}
