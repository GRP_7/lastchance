<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user_recipe;

class RecipeDeletionController extends Controller
{
	public function delete(Request $request)
	{
		// Make sure the CSRF token is excluded when getting the checked recipes
		$input = $request->except('_token');
		$id = session('id');

		foreach ($input as $i) {
			user_recipe::where('Recipe_URL', $i)
				->where('Owner_ID', $id)
				->delete();
		}

		return redirect('/recipes');
	}
}
