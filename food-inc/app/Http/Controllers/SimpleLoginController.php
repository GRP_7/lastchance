<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\user_role;

class SimpleLoginController extends Controller
{
	public function Login(Request $request)
	{
		$username = $request->input('Username');
		$password = $request->input('Password');

		try {
			$user_row = user::where('Username', $username)->
					  where('Password', $password)->
					  firstOrFail();
		} catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
			return view('login', ['error' => true]);
		}

		$role = $user_row->User_Role;
		$roles_row = user_role::where('Role_ID', $role)->first();

		$clearance_level = $roles_row->{'Clearance Level'};
		$id = $user_row->User_ID;

		// Global session variables
		session([
			'clearance_level' => $clearance_level,
			'id' => $id,
			'logged_in' => true,
		]);

		return view('index');
	}
}
