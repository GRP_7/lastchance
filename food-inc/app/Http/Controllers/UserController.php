<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
      public $timestamps = false;

      public function create()
      {
          return view('signup');
      }
      public function store(Request $request) {


    request()->validate([
        'Username' => ['required', 'min:4', 'max:32', 'alpha_num'],
        'Password' => ['required', 'min:4', 'max:63']
    ]);

    \App\user::create([
        'Username' => $request->get('Username'),
        'Password' => $request->get('Password'),
        'User_Role' => 4
    ]);

        return redirect('/');
    }
}
