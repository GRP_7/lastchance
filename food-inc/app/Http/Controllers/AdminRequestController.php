<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user;
use App\user_recipe;
use App\pantry;

class AdminRequestController extends Controller
{
	public function request(Request $request)
	{
		$input = $request->except('_token');

		foreach ($input as $i) {
			pantry::where('Owner_ID', $i)
				->delete();
			user_recipe::where('Owner_ID', $i)
				->delete();
			user::where('User_ID', $i)
				->delete();
		}
		return redirect('/admin');
	}
}
