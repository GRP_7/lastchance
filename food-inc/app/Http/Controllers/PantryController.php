<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pantry;

class PantryController extends Controller
{
	// Return a json message
	public function add(Request $request)
	{
		$input = trim($request->input);
		$id = session('id');
		$pantry = new pantry;

		// Prevent long inputs
		if (strlen($input) > 30) {
			return response()->json([
				'message' => 'Ingredient name too long!',
				'status' => false,
				'new' => ""
			]);
		}

		// Allow only alphabetical characters
		if (!ctype_alpha($input)) {
			return response()->json([
				'message' => 'Ingredient names are restricted to alphabetical characters.',
				'statsus' => false,
				'new' => ""
			]);
		}

		// Prevent duplicates
		if ($pantry
			->where('Owner_ID', $id)
			->where('Ingredient', $input)
			->exists()) {
			return response()->json([
				'message' => 'You already have that!',
				'status' => false,
				'new' => $input
			]);
		}

		$pantry->fill([
			'Ingredient' => $input,
			'Owner_ID' => $id
		]);
		$pantry->save();

		return response()->json([
			'message' => 'Added ' . $input,
			'status' => true,
			'new' => $input
		]);
	}

	public function remove(Request $request)
	{
		// Make sure the CSRF token is excluded when getting the checked recipes
		$to_remove = $request->ingredient;
		$id = session('id');

		pantry::where('Ingredient', $to_remove)
			->where('Owner_ID', $id)
			->delete();

		return "success";
	}
}
