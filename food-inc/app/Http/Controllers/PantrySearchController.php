<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class SearchController extends Controller
{
	public function APIsearch(Request $request)
	{
		// Consider a check for both empty searches and $request unset
		// (when GET is used)
		$search_string = $request->input('search');
		$client = new Client([
			// Base URI is used with relative requests
			'base_uri' => 'https://api.edamam.com/search',
			// You can set any number of default request options.
			'timeout'  => 10.0,
		]);

		$app_id='110c85b0';
		$app_key='02678b0080fb13d5ecf7e9ecba7d64bd';
		$max_results=20;

		// This conditional and file is for debugging
		$saved_json_output_file = '/home/flakon/Downloads/output.json';
		if (file_exists($saved_json_output_file)) {
			$json = file_get_contents($saved_json_output_file);
			error_log("bruh");
		} else {
			$response = $client->request('GET',
										 'https://api.edamam.com/search',
										 ['query' => ['q' => $search_string,
													  'app_id' => $app_id,
													  'app_key' => $app_key,
													  'to' => $max_results]
										 ]);
			$json = $response->getBody();
		}

		$json_obj = json_decode($json);
		// See the Edamam API docs for information on what a "hit" is
		$json_hits = $json_obj->{'hits'};

		// Lambda for extracting recipes from hits, that way the blade file is
		// less verbose and error prone
		$extract_recipe = function($hit)
		{
			return $hit->recipe;
		};

		$json_recipe_list = array_map($extract_recipe, $json_hits);
		// Use the global session helper to store the recipe list,
		// as we need to access it during the AJAX call
		session(['api_recipe_results' => $json_recipe_list]);
		return view('search-results');
	}
}
