<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\pantry;

class SearchController extends Controller
{
	// error checking needed (short/long strings, no ingredients in pantry)
	public function APIsearch(Request $request)
	{
		// determine if pantry or manual search was performed
		if ($request->input('pantry')) {
			$id = session('id');
			$pantry = pantry::where('Owner_ID', $id)->get();

			// Extract ingredients from the rows and concatenate them
			$ingredients = $pantry->map(function($r) { return $r->Ingredient; });
			$search_string = $ingredients->implode(" ");
		} else {
			$search_string = $request->input('search');
		}

		$client = new Client([
			// Base URI is used with relative requests
			'base_uri' => 'https://api.edamam.com/search',
			// You can set any number of default request options.
			'timeout'  => 10.0,
		]);

		$app_id='110c85b0';
		$app_key='02678b0080fb13d5ecf7e9ecba7d64bd';
		$max_results=20;

		// This conditional and file is for debugging
		// $saved_json_output_file = '/home/flakon/downloads/output.json';
		if (!empty($saved_json_output_file) && file_exists($saved_json_output_file)) {
			$json = file_get_contents($saved_json_output_file);
		} else {
			$response = $client->request('GET',
										 'https://api.edamam.com/search',
										 ['query' => ['q' => $search_string,
													  'app_id' => $app_id,
													  'app_key' => $app_key,
													  'to' => $max_results]
										 ]);
			$json = $response->getBody();
		}

		$json_obj = json_decode($json);
		// See the Edamam API docs for information on what a "hit" is
		$json_hits = $json_obj->{'hits'};

		// Lambda for extracting recipes from hits, that way the blade file is
		// less verbose and error prone
		$extract_recipe = function($hit)
		{
			return $hit->recipe;
		};

		$json_recipe_list = array_map($extract_recipe, $json_hits);
		// Use the global session helper to store the recipe list,
		// as we need to access it during the AJAX call
		session(['api_recipe_results' => $json_recipe_list]);
		return view('search-results');
	}
}
