<?php

namespace App\Http\Controllers;

use App\user_recipe;
use App\recipe_book;
use App\ingredients_recipe;
use App\pantry;
use App\user;
use App\user_role;

class PagesController extends Controller
{
	public function index()
	{
		return view('index');
	}

	public function recepies()
	{
		return view('recepies');
	}

	public function recepiestorage()
	{
		return view('recepiestorage');
	}

	public function login()
	{
		if (empty(session('logged_in'))) {
			return view('login');
		} else {
			return view('index');
		}
	}

	public function signup()
	{
		return view('signup');
	}

    public function examplerecipe()
    {
        return view('examplerecipe');
    }

	public function recipes()
	{
		if (empty(session('logged_in'))) {
			return view('index');
		}

		$user_id = session('id');

		$user_recipes = user_recipe::where('Owner_ID', $user_id)
					  ->join('recipe_books', 'user_recipes.Recipe_URL', '=', 'recipe_books.URL')
					  ->get();

		// Return early if the user has no recipes
		// Check if recipes is empty in the blade file later
		if ($user_recipes->isEmpty()) {
			return view('recipes');
		}

		$ingredients_rows = ingredients_recipe::all();

		// Array that maps URLS to ingredients
		$url_ingredients_mapping;
		foreach ($user_recipes as $r) {
			$url_ingredients_mapping[$r->Recipe_URL] = ingredients_recipe::where('URL_Recipe',
																				 $r->Recipe_URL)
													 ->get();
		}

		return view('recipes', [
			'recipes' => $user_recipes,
			'ingredients_map' => $url_ingredients_mapping
		]);
	}

	public function pantry()
	{
		if (empty(session('logged_in'))) {
			return view('index');
		}

		$user_id = session('id');

		$user_pantry = pantry::where('Owner_ID', $user_id)->get();

		return view('pantry', ['pantry' => $user_pantry]);
	}

	public function admin()
	{
		if (empty(session('logged_in')) || session('clearance_level') < 10) {
			return view('index');
		}
		$user_id = session('id');
		// Don't display the current user
		$users_with_roles = user::where('User_ID', '!=', $user_id)
						  ->join('user_roles', 'users.User_Role', '=', 'user_roles.Role_ID')
						  ->get();

		return view('admin', ['users' => $users_with_roles]);
	}
}
