<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user_recipe;
use App\recipe_book;
use App\ingredients_recipe;

class AjaxController extends Controller
{
	// This function stores the result in the DB
	// It returns a status message that should be displayed near the
	// button pressed.
	public function post(Request $request)
	{
		if (empty(session('logged_in'))) {
			return "Register to save recipes and unlock more benefits!";
		}
		// Instead of going through hoops to retrieve the recipe list
		// through a parameter, we retrieve it through the global session
		$index = $request->index;
		$recipe_list = session('api_recipe_results');
		$selected_recipe = $recipe_list[$index];

		$url = $selected_recipe->url;
		$label = $selected_recipe->label;
		$image = $selected_recipe->image;
		$calories = $selected_recipe->calories;
		$ingredients = $selected_recipe->ingredientLines;

		$user_recipe_table = new user_recipe;
		$recipe_table = new recipe_book;

		// Debugging
		$user_id = session('id');

		// Consider doing this check when rendering the results page instead,
		// that way the user can easily see if they already have the recipe
		if ($user_recipe_table->
			where('Owner_ID', $user_id)->
			where('Recipe_URL', $url)->
			exists()) {
			return "You already have this recipe!";
		}

		// Add to the recipe book if it doesn't exist
		if ($recipe_table->where('URL', $url)->doesntExist()) {
			try {
				$recipe_table->fill([
					'URL' => $url,
					'Label' => $label,
					'Image' => $image,
					'Calories' => $calories,
				]);
				$recipe_table->save();

				// Add ingredients one by one
				foreach ($ingredients as $ingr) {
					$ingredients_table = new ingredients_recipe;
					$ingredients_table->fill([
						'URL_Recipe' => $url,
						'Ingredients' => $ingr
					]);
					$ingredients_table->save();
				}
			} catch (\Illuminate\Database\QueryException $e) {
				return "Something went wrong - unable to add recipe";
			}
		}

		// Now that we have ensured the recipe is available, add it to the
		// user's saved recipes
		$user_recipe_table->fill([
			'Owner_ID' => $user_id,
			'Recipe_URL' => $url,
			'Available' => 1,
		]);
		$user_recipe_table->save();

		return "Recipe successfully added";
	}
}
