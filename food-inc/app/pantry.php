<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pantry extends Model
{
    public $timestamps = false;

	protected $fillable = [
		'Ingredient',
		'Owner_ID'
	];
}
