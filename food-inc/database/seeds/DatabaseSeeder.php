<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        /* Seeding user_roles */
        DB::table('user_roles')->insert([
        'Role' => 'Admin',
        'Clearance Level' => 10
        ]);
        DB::table('user_roles')->insert([
        'Role' => 'Moderator',
        'Clearance Level' => 7
        ]);
        DB::table('user_roles')->insert([
        'Role' => 'Premium',
        'Clearance Level' => 5
        ]);
        DB::table('user_roles')->insert([
        'Role' => 'Regular',
        'Clearance Level' => 3
        ]);
        DB::table('user_roles')->insert([
        'Role' => 'Child',
        'Clearance Level' => 1
        ]);

        /* Seeding some users */
        DB::table('users')->insert([
        'Username' => 'Admin',
        'Password' => 'Admin',
        'User_Role' => '1'
        ]);
        DB::table('users')->insert([
        'Username' => 'Moderator',
        'Password' => 'Moderator',
        'User_Role' => '2'
        ]);
        DB::table('users')->insert([
        'Username' => 'Premium',
        'Password' => 'Premium',
        'User_Role' => '3'
        ]);
        DB::table('users')->insert([
        'Username' => 'Regular',
        'Password' => 'Regular',
        'User_Role' => '4'
        ]);
        DB::table('users')->insert([
        'Username' => 'Child',
        'Password' => 'Child',
        'User_Role' => '5'
        ]);

        /* Seeding some pantries */
        DB::table('pantries')->insert([
        'Ingredient' => 'Chili',
        'Owner_ID' => 4,
        ]);
        DB::table('pantries')->insert([
        'Ingredient' => 'Tomato',
        'Owner_ID' => 4,
        ]);
        DB::table('pantries')->insert([
        'Ingredient' => 'Soy',
        'Owner_ID' => 4
        ]);
        DB::table('pantries')->insert([
        'Ingredient' => 'Chili',
        'Owner_ID' => 3,
        ]);
        DB::table('pantries')->insert([
        'Ingredient' => 'Tomato',
        'Owner_ID' => 3,
        ]);
        DB::table('pantries')->insert([
        'Ingredient' => 'Soy',
        'Owner_ID' => 3
        ]);

        /* Seeding some recipe_books */
        DB::table('recipe_books')->insert([
        'URL' => 'http://www.davidlebovitz.com/2012/12/chicken-teriyaki-recipe-japanese-farm-food/',
        'Label' => 'Teriyaki Chicken',
        'Image' => 'https://www.edamam.com/web-img/262/262b4353ca25074178ead2a07cdf7dc1.jpg',
        'Calories' => '2253.101981306866',
        ]);

        /* Seeding a recipe */
        DB::table('ingredients_recipes')->insert([
        'URL_Recipe' => 'http://www.davidlebovitz.com/2012/12/chicken-teriyaki-recipe-japanese-farm-food/',
        'Ingredients' => '1/2 cup (125ml) mirin'
        ]);
        DB::table('ingredients_recipes')->insert([
        'URL_Recipe' => 'http://www.davidlebovitz.com/2012/12/chicken-teriyaki-recipe-japanese-farm-food/',
        'Ingredients' => '1/2 cup (125ml) soy sauce'
        ]);
        DB::table('ingredients_recipes')->insert([
        'URL_Recipe' => 'http://www.davidlebovitz.com/2012/12/chicken-teriyaki-recipe-japanese-farm-food/',
        'Ingredients' => 'One 2-inch (5cm) piece of fresh ginger, peeled and grated'
        ]);
        DB::table('ingredients_recipes')->insert([
        'URL_Recipe' => 'http://www.davidlebovitz.com/2012/12/chicken-teriyaki-recipe-japanese-farm-food/',
        'Ingredients' => '2-pounds (900g) boneless chicken thighs (4-8 thighs, depending on size)'
        ]);

        /* Seeding some user_recipes */
        DB::table('user_recipes')->insert([
        'Owner_ID' => '5',
        'Recipe_URL' => 'http://www.davidlebovitz.com/2012/12/chicken-teriyaki-recipe-japanese-farm-food/',
        'Available' => '1'
        ]);
    }
}
