<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserRecipesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_recipes', function(Blueprint $table)
		{
			$table->increments('Entry');
			$table->integer('Owner_ID')->unsigned();
			$table->string('Recipe_URL')->index('Recipe_URL');
            $table->foreign('Owner_ID')->references('User_ID')->on('users')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('Recipe_URL')->references('URL')->on('recipe_books')->onDelete('restrict')->onUpdate('restrict');
			$table->boolean('Available');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_recipes');
	}

}
