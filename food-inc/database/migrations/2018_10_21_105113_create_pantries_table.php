<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePantriesTable extends Migration {
	public $timestamps = false;
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pantries', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('Ingredient', 150);
			$table->integer('Owner_ID')->unsigned();
            $table->foreign('Owner_ID')->references('User_ID')->on('users')->onDelete('restrict')->onUpdate('restrict');
			// $table->primary(['Ingredient','Owner_ID']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pantries');
	}

}
