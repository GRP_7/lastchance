<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {
public $timestamps = false;
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() 
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->string('Username', 50)->unique('name');
			$table->string('Password', 50);
			$table->integer('User_Role')->unsigned();
			$table->increments('User_ID');
            $table->foreign('User_Role')->references('Role_ID')->on('user_roles')->onDelete('restrict')->onUpdate('restrict');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
