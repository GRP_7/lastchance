<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIngredientsRecipesTable extends Migration {
	public $timestamps = false;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('ingredients_recipes', function(Blueprint $table)
        {
			$table->increments('id');
            $table->string('URL_Recipe', 200);
            $table->string('Ingredients', 200);
            $table->foreign('URL_Recipe')->references('URL')->on('recipe_books')->onDelete('restrict')->onUpdate('restrict');
			// $table->primary(['URL_Recipe','Ingredients']);
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ingredients_recipe');
    }
}
