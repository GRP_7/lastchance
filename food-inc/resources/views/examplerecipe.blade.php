<link href="{{ URL::asset('/css/pantry.css') }}" rel="stylesheet">
@extends('layout')
@section('content')
    <div class="content">
        <div class="module">
			<h1>Spaghetti Amatriciana</h1>
            <img src="/images/examplepic.jpg">
            <br>
            <br>
	        <a href="https://norecipes.com/spaghetti-amatriciana">Click for instructions</a>
            <br>
            <br>
            150 grams Guanciale cut into 1/4″ batons 
            <br>
            1 chili pepper smashed 
            <br>
            1 medium shallot finely minced
            <br>
            1/4 cup red wine 
            <br>
            375 grams whole stewed tomatoes (preferably from San Marzano) 
            <br>
            1 tablespoon tomato paste 
            <br>
            1/2 teaspoon salt 
            <br>
            30 grams Pecorino Romano finely grated 
            <br>
            250 grams spaghetti 
            <br>
            <h2>Calories</h2>
            @if (session('clearance_level') >= 5)
                925 CALORIES / SERVING
            @else
                Only premium users can view calories!
                <br>
            @endif
        </div>
    </div>
@endsection
