@extends('layout')

@section('content')
    <div class="content">
        <div class="module"><h1>Sign Up</h1>
            <form method="POST" action="/signup">
                @csrf
                <input type="signuptext" name="Username" placeholder="Username" value="{{ old('Username') }}" required>
                <input type="password" name="Password" placeholder="Password" required>
                <button type="submit">Sign up</button>
            </form>
            <div>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
            </div>
        </div>
    </div>
@endsection
