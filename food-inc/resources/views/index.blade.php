@extends('layout')
@section('content')
<head><script src="/js/slide.js"></script></head>
<body onload="slideshow()">
    <div class="content">
        <div class="indexModule">
        	<div class="text">
				<h1>Welcome,</h1>
	        	Here you can search for recipes and save your favorites in a recipe 
	        	book that you can revisit. You can signup and become a premium user
	        	for a small sum, this will give you more information about your
	        	recipes.
	        	<br><br>
	        	You can fill your pantry with the ingredients you currently have and we will
	        	give you some ideas for what you can cook up, so what are you waiting for?
	        	<br><br>
	        	<br><br>
                Click <a href="/examplerecipe">here</a> for a example recipe. 
	        	
        	</div>
            <div class="pic">
        		    <div class="slides">
                    <img src="/images/chickenslide.jpg">
                    </div>
                    <div class="slides">
                    <img src="/images/pasta.jpg">
                    </div>
                    <div class="slides">
                    <img src="/images/slidebbq.jpg">
                    </div>
                    <div class="slides">
                    <img src="/images/slideramen.jpg">
                    </div>
        	</div>
        </div>
    </div>
    </body>
@endsection
