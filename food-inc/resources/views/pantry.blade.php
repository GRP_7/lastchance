<link href="{{ URL::asset('/css/pantry.css') }}" rel="stylesheet">
@extends('layout')
@section('content')
    <div class="content">
        <div class="module">
            <h1>Pantry</h1>
            <div id="ingredient-list">
                @foreach ($pantry as $p)
                    <div class="ingredient-container">
                        {{ $p->Ingredient }}
                        <input id="remove-button"
                               type="button"
                               onclick="ajaxRemoveIngredient('{{ csrf_token() }}', this.parentElement, '{{ $p->Ingredient }}')"
                               value="Remove">
                    </div>
                @endforeach
            </div>
            <br>
            <input id="input"
                   type="text"
                   placeholder="Add ingredient..."
                   name="input">
            <input id="button"
                   type="button"
                   onclick="ajaxAddIngredient('{{ csrf_token() }}', this)"
                   value="Add" style="float: none;">
            <br>
            <br>
            <span id="response"></span>
            <br>
            <br>
            <form action="/search-results"
                  method="post">
                @csrf
                <!-- No value - this is simply used by the SearchController
                     to check if the pantry or manual search was used -->
                <input type="hidden" name="pantry">
                <br>
                <input type="submit" name="pantry" value="Search with ingredients">
            </form>
        </div>
    </div>
    <script src="/js/pantry.js"></script>
@endsection
