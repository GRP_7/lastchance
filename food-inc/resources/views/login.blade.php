@extends('layout')

@section('content')
    <div class="content">
        <div class="module"><h1>Log in</h1>
            <form method="POST" action="/login">
                @csrf
                <input type="signuptext" name="Username" placeholder="Username" required>
                <input type="password" name="Password" placeholder="Password" required>
                <button type="submit">Log in</button>
            </form>
            @if(!empty($error))
                <span>Invalid username or password.</span>
            @endif
        </div>
    </div>
@endsection
