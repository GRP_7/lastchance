<!DOCTYPE html>
<html>
    <head >
        <meta charset="UTF-8" />
        <title>Food Inc.</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" type="text/css" href="/css/app.css" />
		<link rel="shortcut icon" type = "image/gif" href="/images/logo.gif">
    </head>
    <body>
        <section id="wrap">
            <div class="header" style="background-image: url('/images/headbg.jpg');">
                <h1>Food Inc.</h1>
                <p>The only place for all your recipes.</p>
            </div>
            <br>
            <div id="navbar">
                <a class= "logo" href="/"><image src="/images/logo.gif" width="47px" height="40px"></a>
                @if (empty(session('logged_in')))
                    <a href="/login">Log in</a>
                    <a href="/signup/create">Sign Up</a>
                @else
                    <a href="/recipes">Recipes</a>
                    <a href="/pantry">Pantry</a>
                    <a href="/logout">Log out</a>
                    @if (session('clearance_level') >= 10)
                        <a href="/admin">Admin</a>
                    @endif
                @endif
                <form action="{{ action('SearchController@APIsearch') }}"
                      method="post">
                    <!-- Verify token - otherwise error code 419 expiration errors happen -->
                    @csrf
                    <input id="query"
                           type="text"
                           placeholder="Search for recipes.."
                           name="search" required minlength="3">
                           <!-- No empty strings ^-->
                    <!-- ^searchbox to take foodquery to the API -->
                    <input id="srchbtn" type="submit" name="submit" value="Go">
                </form>
            </div>

            @yield('content')
            <!-- Yields our content
                 from the different pages-->

            <div class="footer">
                <p>Copyright Food Inc. 2018</p>
            </div>
        </section>
        <script src="/js/navbar.js"></script>
    </body>
</html>
