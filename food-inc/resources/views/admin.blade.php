@extends('layout')
@section('content')
    <div class="content">
        <div class="adminModule"><h1>Delete a user</h1>
            <form method="POST"
                  action="/admin-request" >
                @csrf
                <table id="users" align="center">
                    <tr>
                        <th>user</th>
                        <th>role</th>
                    </tr>
                    @foreach ($users as $u)
                        <tr>
                            <td>{{ $u->Username }}</td>
                            <td>{{ $u->Role }}</td>
                            <td>
                                <input type="checkbox"
                                   name="deletion{{ $loop->index }}"
                                   value='{{ $u->User_ID }}'
                                   class="delete-mark"
                                   onclick="enableRequestAndShow()">
                            </td>
                        </tr>
                    @endforeach
                </table>
                <br>
                <button id="confirm-action"
                        type="submit"
                        disabled>
                    Delete
                </button>
                <span id="marktext"></span>
            </form>
        </div>
    </div>
    <script src="/js/recipes.js"></script>
    <script src="/js/admin.js">
     document.addEventListener('DOMContentLoaded', enableRequestAndShow);
    </script>
@endsection
