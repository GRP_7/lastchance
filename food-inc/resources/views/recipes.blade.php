<link href="{{ URL::asset('/css/recipes.css') }}" rel="stylesheet">
@extends('layout')
@section('content')
    <div class="content">
        @if (!empty($recipes))
            <form method="POST"
                  action="/delete-recipes" >
                @csrf
                @foreach ($recipes as $r)
                    <div class="module">
                        <input type="checkbox"
                               name="deletion{{ $loop->index }}"
                               value='{{ $r->Recipe_URL }}'
                               class="delete-mark"
                               onclick="enableDeleteAndShow()">
                        Mark for deletion
                        <h1>{{ $r->Label }}</h1>
                        <img src="{{ $r->Image }}"/>
                        <br>
                        <a href="{{ $r->URL }}" id="instructions" >Click for instructions</a>
                        <br>
                        <br>
                        <div id="ingredients-container">
                            @foreach ($ingredients_map[$r->Recipe_URL] as $i)
                                <span class="ingredient">
                                    {{ $i->Ingredients }}
                                </span>
                                <br>
                            @endforeach
                        </div>
                        <br>
                        <h2>Calories</h2>
                        @if (session('clearance_level') >= 5)
                            {{ $r->Calories }}
                        @else
                            Only premium users can view calories!
                        @endif
                        <br>
                    </div>
                @endforeach
                <span id="marktext"></span>
                <br>
                <br>
                <button id="confirm-delete"
                        type="submit"
                        disabled>
                    Delete
                </button>
                <!-- Shows how many recipes are marked -->
            </form>
        @else
            Add some recipes by searching!
        @endif
    </div>
    <script src="/js/recipes.js">
     window.addEventListener('DOMContentLoaded', enableDeleteAndShow);
    </script>
@endsection
