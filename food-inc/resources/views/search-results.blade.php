<link href="{{ URL::asset('/css/pantry.css') }}" rel="stylesheet">
@extends('layout')
@section('content')
    <div class="content">
        @foreach (session('api_recipe_results') as $res)
            <button type="show"
                onclick="showRecipe('{{ $res->{'label'} }}-{{ $loop->index }}')">
                Show {{ $res->{'label'} }}
            </button>
            <br>
            <div class="module" id="{{ $res->{'label'} }}-{{ $loop->index }}"
                                style="display: none">
                <h1>{{ $res->{'label'} }}</h1>
                <br>
                <img src="{{ $res->{'image'} }}" alt="[Image of food]">
                <br>
                <a href="{{ $res->{'url'} }}">
                    Click for instructions
                </a>
                <h2>Ingredients</h2>
                    @foreach ($res->{'ingredientLines'} as $ingredient)
                            {{ $ingredient }}
                            <br>
                    @endforeach
                <br>
                <h2>Calories</h2>
                @if (session('clearance_level') >= 5)
                    {{ $res->calories }}
                @else
                    Only premium users can view calories!
                    <br>
                @endif
                <br>
                <br>
                <!-- Saving recipes -->
                <button type="submit" onclick='ajaxAddRecipe("{{ csrf_token() }}", {{ $loop->index }})'
                        id="button{{ $loop->index }}">
                    Save recipe
                </button>
                <br>
                <!-- Create divs that show the result of feedback when pressing a button -->
                <div id="result{{ $loop->index }}"></div>
            </div>
        @endforeach
        @if(empty(session('api_recipe_results')))
            <h2 >No recipes found</h2>
        @endif
        <script src="/js/search.js"></script>
    </div>
@endsection
