<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'PagesController@index');


Route::get('/recepies', 'PagesController@recepies');

Route::get('/recepiestorage', 'PagesController@recepiestorage');

Route::get('/info', 'PagesController@info');

Route::get('/login', 'PagesController@login');
Route::post('/login', 'SimpleLoginController@Login');
Route::get('/logout', 'LogOutController@logout');

Route::get('/recipes', "PagesController@recipes");
Route::get('/pantry', "PagesController@pantry");

Route::get('/signup', 'PagesController@signup');
Route::resource('/signup', 'UserController');

Route::get('/search-results', 'SearchController@APIsearch');
Route::post('/search-results', 'SearchController@APIsearch');

Route::post('/post-ajax', 'AjaxController@post');

Route::post('/delete-recipes', 'RecipeDeletionController@delete');

Route::post('/add-ingredient', 'PantryController@add');
Route::post('/remove-ingredient', 'PantryController@remove');

Route::get('/admin', 'PagesController@admin');
Route::post('/admin-request', 'AdminRequestController@request');

Route::get('/examplerecipe', 'PagesController@examplerecipe');
