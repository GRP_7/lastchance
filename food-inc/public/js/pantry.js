// Params:
// token: CSRF token
// ingredient: Ingredient string
function createIngredientContainer(token, ingredient)
{
    let div = document.createElement('div');
    let button = document.createElement('input');
    // Use a template literal to build this string
    let btn_onclick_str =
        `ajaxRemoveIngredient('${token}', this.parentElement, '${ingredient}')`;

    div.className = "ingredient-container";
    div.innerHTML = ingredient;

    button.setAttribute('type', 'button');
    button.setAttribute('onclick', btn_onclick_str);
    button.setAttribute('value', "remove");
    div.appendChild(button);

    return div;
}

// PARAMS:
// token: CSRF token
// button: button pressed that calls function
// Adds an ingredient to the database and show it in the view if it succeeds.
// Otherwise an error is shown.
// JSON FORMAT:
// message : Appropriate status message to display
// status : Boolean that is true if the request was successful
// new : new ingredient name (the input is trimmed on the backend
//       and therefore not necessarily equal to the input)
function ajaxAddIngredient(token, button)
{
    let xhr = new XMLHttpRequest();
    let textinput = document.getElementById('input');
    let string = textinput.value;

    button.disabled = true;
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let response = JSON.parse(this.responseText);
            let ingredient = response.new;
            let container = createIngredientContainer(token, ingredient);

            document.getElementById('response').innerHTML = response.message;
            button.disabled = false;
            textinput.value = "";
            if (response.status) {
                document.getElementById('ingredient-list').appendChild(container);
            }
        }
    };
    xhr.open("POST", "/add-ingredient", true);
    // Add CSRF token to the http header
    xhr.setRequestHeader("x-csrf-token", token);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("input=" + string);
}

// Params:
// token: CSRF token
// entry: Element to remove (pass the parent element of the button pressed)
// ingredient: Ingredient to remove from the database
function ajaxRemoveIngredient(token, entry, ingredient)
{
    let xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            entry.remove();
        }
    };
    xhr.open("POST", "/remove-ingredient", true);
    // Add CSRF token to the http header
    xhr.setRequestHeader("x-csrf-token", token);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // Pass the index as a POST variable
    xhr.send("ingredient=" + ingredient);
}
