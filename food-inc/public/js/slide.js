let sliderIndex = 1; // keeps track of the slider index

function slideshow() {
    let x = document.getElementsByClassName("slides"); // gets element with the class "slides"
    for (let i = 0; i < x.length; i++) {
        x[i].style.display = "none"; // hides other pages
    }

    if (sliderIndex > x.length) { sliderIndex = 1; } // resets slideshow when the index goes over the number of slides
    x[sliderIndex - 1].style.display = "block"; // displays the page
    setTimeout(slideshow, 1000); // CHANGE THIS TO 30000 BEFORE PRESENTING
    sliderIndex++; // goes to the next image
}
