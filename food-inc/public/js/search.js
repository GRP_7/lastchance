// Function for showing/hiding the recipes
function showRecipe(recipeId) {
    var recipe = document.getElementById(recipeId);
    if (recipe.style.display === 'none') {
        recipe.style.display = 'block';
    } else {
        recipe.style.display = 'none';
    }
}
// Parameters:
// token: CSRF token
// index: nth index of button pressed
function ajaxAddRecipe(token, index)
{
    let xhr = new XMLHttpRequest();
    let params = "index=" + index;

    document.getElementById("button" + index).disabled = true;
    xhr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("result" + index).innerHTML = this.responseText;
        }
    };
    xhr.open("POST", "/post-ajax", true);
    // Add CSRF token to the http header
    xhr.setRequestHeader("x-csrf-token", token);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // Pass the index as a POST variable
    xhr.send(params);
}
