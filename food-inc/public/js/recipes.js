// Enables the delete button and shows the numbers of marked checkboxes
function enableDeleteAndShow()
{
    let checkboxes = document.getElementsByClassName('delete-mark');
    let confirmButton = document.getElementById('confirm-delete');
    let marktext = document.getElementById('marktext');
    let checked = Array.prototype.filter.call(checkboxes,
                                              function(cb) {
                                                  return cb.checked;
                                              });

    if (checked.length == 1) {
        confirmButton.disabled = false;
        marktext.innerHTML = "1 recipe marked for deletion.";
    } else if (checked.length) {
        confirmButton.disabled = false;
        marktext.innerHTML = checked.length + " recipes marked for deletion.";
    } else {
        confirmButton.disabled = true;
        marktext.innerHTML = "No recipes marked for deletion.";
    }
}
