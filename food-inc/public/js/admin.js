// Enables the delete button and shows the numbers of marked checkboxes
// Similar to the function used for recipes.
function enableRequestAndShow()
{
    let checkboxes = document.getElementsByClassName('delete-mark');
    let confirmButton = document.getElementById('confirm-action');
    let marktext = document.getElementById('marktext');
    let checked = Array.prototype.filter.call(checkboxes,
                                              function(cb) {
                                                  return cb.checked;
                                              });

    if (checked.length == 1) {
        confirmButton.disabled = false;
        marktext.innerHTML = "1 user marked for deletion.";
    } else if (checked.length) {
        confirmButton.disabled = false;
        marktext.innerHTML = checked.length + " users marked for deletion.";
    } else {
        confirmButton.disabled = true;
        marktext.innerHTML = "No users marked for deletion.";
    }
}
